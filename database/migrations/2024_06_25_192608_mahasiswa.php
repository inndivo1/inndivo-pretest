<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('mahasiswa', function (Blueprint $table) {
            $table->char('nim', length: 8)->primary();
            $table->string('nama_mahasiswa', length: 50);
            $table->string('tempat_lahir', length: 50);
            $table->date('tanggal_lahir');
            $table->string('alamat', length: 100);
            $table->char('kode_jurusan', length: 2);
            $table->foreign('kode_jurusan')->references('kode_jurusan')->on('jurusan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('mahasiswa');
    }
};
