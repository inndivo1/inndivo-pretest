<?php

use App\Http\Controllers\JurusanController;
use App\Http\Controllers\MahasiswaController;
use Illuminate\Support\Facades\Route;

Route::controller(MahasiswaController::class)->group(function () {
    Route::get('/', 'index')->name('index-mahasiswa');
    Route::get('/mahasiswa/input', 'create')->name('create-mahasiswa');
    Route::post('/mahasiswa/input', 'store')->name('store-mahasiswa');
    Route::get('/mahasiswa/update/{id}', 'edit')->name('edit-mahasiswa');
    Route::post('/mahasiswa/update/{id}', 'update')->name('update-mahasiswa');
    Route::get('/mahasiswa/delete/{id}', 'destroy')->name('delete-mahasiswa');
});
Route::controller(JurusanController::class)->group(function () {
    Route::get('/jurusan', 'index')->name('index-jurusan');
    Route::get('/jurusan/input', 'create')->name('create-jurusan');
    Route::post('/jurusan/input', 'store')->name('store-jurusan');
    Route::get('/jurusan/update/{id}', 'edit')->name('edit-jurusan');
    Route::post('/jurusan/update/{id}', 'update')->name('update-jurusan');
    Route::get('/jurusan/delete/{id}', 'destroy')->name('delete-jurusan');
});

