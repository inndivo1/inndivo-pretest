<?php

namespace App\Http\Controllers;

use App\Models\Jurusan;
use App\Models\Mahasiswa;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class MahasiswaController extends Controller
{
    private $msg = [
        'required' => 'Harap mengisi inputan :attribute!',
    ];

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Mahasiswa::all();
        return view('welcome', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $jurusan = Jurusan::all();
        return view('mahasiswa.input-mahasiswa', compact('jurusan'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nama_mahasiswa' => 'required|max:50',
            'tempat_lahir' => 'required|max:50',
            'tanggal_lahir' => 'required|date',
            'alamat' => 'required|max:100',
            'kode_jurusan' => 'required',
        ], $this->msg);

        $new_data = new Mahasiswa();
        $new_data->nim = strtoupper(Str::random(2));
        $new_data->nama_mahasiswa = $request->nama_mahasiswa;
        $new_data->tempat_lahir = $request->tempat_lahir;
        $new_data->tanggal_lahir = $request->tanggal_lahir;
        $new_data->alamat = $request->alamat;
        $new_data->kode_jurusan = $request->kode_jurusan;
        $new_data->save();

        return redirect()->route('index-mahasiswa')->with('pesan', 'Data mahasiswa baru berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     */
    public function show(Mahasiswa $mahasiswa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $data = Mahasiswa::where('nim', $id)->first();
        $jurusan = Jurusan::all();
        return view('mahasiswa.update-mahasiswa', compact('data', 'jurusan'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'nama_mahasiswa' => 'required|max:50',
            'tempat_lahir' => 'required|max:50',
            'tanggal_lahir' => 'required|date',
            'alamat' => 'required|max:100',
            'kode_jurusan' => 'required',
        ], $this->msg);

        $new_data = Mahasiswa::where('nim', $id)->update([
            'nama_mahasiswa' => $request->nama_mahasiswa, 
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'alamat' => $request->alamat,
            'kode_jurusan' => $request->kode_jurusan
            ]);

        return redirect()->route('index-mahasiswa')->with('pesan', 'Data mahasiswa berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        Mahasiswa::where('nim', $id)->delete();
        return redirect()->route('index-mahasiswa')->with('pesan', 'Data mahasiswa berhasil dihapus!');
    }
}
