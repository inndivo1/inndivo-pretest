<?php

namespace App\Http\Controllers;

use App\Models\Jurusan;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class JurusanController extends Controller
{
    private $msg = [
        'required' => 'Harap mengisi inputan :attribute!',
    ];

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Jurusan::all();
        return view('jurusan.view-jurusan', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        return view('jurusan.input-jurusan');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nama_jurusan' => 'required|max:50'
        ], $this->msg);

        $new_data = new Jurusan();
        $new_data->kode_jurusan = strtoupper(Str::random(2));
        $new_data->nama_jurusan = $request->nama_jurusan;
        $new_data->save();

        return redirect()->route('index-jurusan')->with('pesan', 'Data jurusan baru berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     */
    public function show(Jurusan $jurusan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $data = Jurusan::where('kode_jurusan', $id)->first();
        return view('index-jurusan', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'nama_jurusan' => 'required|max:50'
        ], $this->msg);

        $data = Jurusan::where('kode_jurusan', $id)
        ->update(['nama_jurusan' => $request->nama_jurusan]);

        return redirect()->route('index-jurusan')->with('pesan', 'Data jurusan baru berhasil ditambahkan!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        Jurusan::where('kode_jurusan', $id)->delete();
        return redirect()->route('index-jurusan')->with('pesan', 'Data jurusan berhasil dihapus!');
    }
}
