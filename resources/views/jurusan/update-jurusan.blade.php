@extends('layout')
@section('content')
    <main>
        <form action="{{route('update-jurusan', $data->kode_jurusan)}}" method="POST">
            @csrf
            <div class="mb-3 row">
                <label for="nama-jurusan" class="">Nama Jurusan</label>
                <div class="col-sm-8">
                    <input type="text" name="nama_jurusan" class="nama_jurusan" id="nama_jurusan" placeholder="Tulis nama jurusan dengan lengkap" value="{{$data->nama_jurusan}}">
                </div>
            </div>
            <button id="btn-save" class="w-100 mt-3 py-2 bg-yellow-5 text-black border-0 rounded-2">Simpan</button>
        </form>
    </main>

@endsection