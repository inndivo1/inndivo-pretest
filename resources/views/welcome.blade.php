@extends('layout')
@section('content')
<main>
    <h2 class="student-title">Daftar Mahasiswa</h2>
    <center><div class="line"></div></center>
    @if(empty($data))
        <center>
            <div>
                <p>Data tidak ditemukan!</p>
                <img src="{{ asset('assets/sad.png') }}" class="sad-img" alt="image-sad">
            </div>
        </center>
    @else
    <center>
        <table>
            <tr>
                <th>NIM</th>
                <th>Nama</th>
                <th>Tempat Lahir</th>
                <th>Tanggal Lahir</th>
                <th>Alamat</th>
                <th>Kode Jurusan</th>
                <th>Aksi</th>
            </tr>

            @foreach ($data as $item)
            <tr>
                <td>{{$item->nim}}</td>
                <td>{{$item->nama_mahasiswa}}</td>
                <td>{{$item->tempat_lahir}}</td>
                <td>{{$item->tanggal_lahir}}</td>
                <td>{{$item->alamat}}</td>
                <td>{{$item->kode_jurusan}}</td>
                <td>
                    <div class="actions">
                        <a href="{{ route('edit-mahasiswa', $item->nim) }}" class=""><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                        </svg></a>
                        <a href="{{ route('delete-mahasiswa', $item->nim) }}" class=""><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z"/>
                            <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z"/>
                        </svg></a>
                    </div>
                </td>
            </tr>
        @endforeach
    </table>

    @endif
    <a href="{{route('create-mahasiswa')}}" class="add-student">Tambah Data Mahasiswa</a>
    </center>
</main>
@endsection