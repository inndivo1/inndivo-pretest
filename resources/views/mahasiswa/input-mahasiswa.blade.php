@extends('layout')
@section('content')
    <main>
        @if (count($errors) > 0)
        <ul class="alert alert-danger mb-3">
            @foreach ($errors->all() as $error)
            <li>{{$error}}</li>
            @endforeach
        </ul>
        @endif
        <form action="{{route('store-mahasiswa')}}" method="POST">
            @csrf
            <div class="mb-3 row">
                <label for="nama-mahasiswa" class="">Nama Mahasiswa</label>
                <div class="col-sm-8">
                    <input type="text" name="nama_mahasiswa" class="form-control" id="nama-mahasiswa" placeholder="Tulis nama lengkap Anda">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="tempat-lahir" class="col-sm-4 col-form-label w-bold">Tempat Lahir</label>
                <div class="col-sm-8">
                    <input type="text" name="tempat_lahir" class="form-control" id="tempat-lahir" placeholder="Tulis daerah Anda lahir">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="tanggal-lahir" class="col-sm-4 col-form-label w-bold">Tempat Lahir</label>
                <div class="col-sm-8">
                    <input type="date" name="tanggal_lahir" class="form-control" id="tanggal-lahir" placeholder="Tulis tanggal Anda lahir">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="alamat" class="">Alamat</label>
                <div class="col-sm-8">
                    <input type="text" name="alamat" class="form-control" id="alamat" placeholder="Tulis alamat lokasi Anda tinggal saat ini">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="kode-jurusan" class="">Kode Jurusan</label>
                <div class="col-sm-8 ">
                    <select class="form-select" id="kode-jurusan" name="kode_jurusan">
                        <option class="" value='' >Pilih jurusan</option>
                        @foreach($jurusan as $item)
                        <option class="" value='{{$item->kode_jurusan}}' >{{$item->nama_jurusan}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <button id="btn-save" class="w-100 mt-3 py-2 bg-yellow-5 text-black border-0 rounded-2">Simpan</button>
        </form>
    </main>
@endsection