<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Inndivo-Project</title>
        <link href="{{ asset('assets/style.css') }}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <nav class="nav-box">
            <div class="to-home">Mahasiswa<span style="color: #6d6875;">.</span>id</div>
            <div class="navbar">
                <a href="/" class="nav-item">Daftar Mahasiswa</a>
                <a href="/jurusan" class="nav-item">Daftar Jurusan</a>
            </div>
        </nav>
        @yield('content')
        <footer>
            <p> &nbsp; Copyright 2024</p>
            <p>Created By : <span><a href="mailto:luthfianisaaz339@gmail.com" class="email">Luthfia Nisa Azzahra</a></span></p>
        </footer>
    </body>
</html>