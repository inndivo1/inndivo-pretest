# inndivo-pretest



## Getting started

This repository was created to fulfill the pretest internship assignment at Inndivo. Within this repository, there is a menu for performing CRUD (Create, Read, Update, Delete) operations on student data.

## Prerequisites
Ensure you have the following installed on your system:

- PHP (version 8 or higher)
- Composer
- MySQL or any other database you will be using

## Access the Project

- [ ] Clone the Git repository with the following command:

```
git remote add origin https://gitlab.com/inndivo1/inndivo-pretest.git
```
- [ ] Install Composer Dependencies
```
composer install
```
- [ ] Create a Copy of the .env File
```
cp .env.example .env
```
- [ ] Generate an Application Key
``` 
php artisan key:generate 
```
- [ ] Configure the .env File
``` 
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=inndivo
DB_USERNAME=root
DB_PASSWORD=
```
- [ ] Run Database Migrations
```
php artisan migrate
```
- [ ] Start the Laravel development server
```
php artisan serve
```
